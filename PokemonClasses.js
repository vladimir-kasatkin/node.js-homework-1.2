"use strict";

class Pokemon {
  constructor (name, level) {
    this.name = name;
    this.level = level;
  }

  show () {    // выводим информацию по покемону
    console.log(`покемон: ${this.name}, уровень = ${this.level}`);
  }

  valueOf() { return this.level;}  // переопределяем valueOf, чтобы Math.max - отработала сравнения\сортировку между свойством level объектов
}


class PokemonList extends Array {
  add(name, level) {    // добавляем покемона
    this.push(new Pokemon(name, level));
  }

  show(strTitle){   // показываем информацию по покемонам и количество
    console.log(`    *** ${strTitle} ***`);
    this.forEach(objPokemon => objPokemon.show());
    console.log(`количество: ${this.length}`);
    console.log(" ------------------------------- \n");
  }

  max() {    // вычисляем максимальный уровень ( число ), далее фильтруем покемонов по совпадению уровня с максимумом, возвращаем первое совпадение
    let maxlevel = Math.max(...this);
    return this.filter(objPokemon => objPokemon.level == maxlevel )[0];
    }
}

module.exports = {PokemonList, Pokemon};
