
"use strict";

const hidenseek = require('./hidenseek.js');
const pc = require('./PokemonClasses.js');
const fs = require('fs');

// инициализируем начальный список покемонов
let objPokemonListToHide = new pc.PokemonList(
  new pc.Pokemon("Коратта",10),
  new pc.Pokemon("Фусигиданэ",12),
  new pc.Pokemon("Хитокагэ",13),
  new pc.Pokemon("Камэккусу",101),
  new pc.Pokemon("Торансэру",123),
  new pc.Pokemon("Онисудзумэ", 11) );
objPokemonListToHide.show("начальный набор покемонов");





// вызываем функцию, которая прячет покемонов
hidenseek.hide('./tmp2', objPokemonListToHide, (objHiddenPokemons) => {
  objHiddenPokemons.show("список спрятанных покемонов ( вернула функция )");

  hidenseek.seek("./tmp2") // вызов seek ( уже не через callback )
    .then(objFoundPokemonList => objFoundPokemonList.show("результат работы seek"))
    .catch( err => console.log(err));
});
