"use strict";

const fs = require('fs');
const pc = require('./PokemonClasses.js');
const arrFolders = ['01','02','03','04','05','06','07','08','09','10'];

const hide = (strStartPath, objPokemonList, callback) => {
  let arrPokemonsToHide = getPokemonsToHide(objPokemonList); // получаем список покемонов, которых будем прятать
  let nFoldersCreated= 0;
  fs.mkdir(strStartPath, (err) => { // создаем корневую папку для 01-10
    if (err && (err.code !== 'EEXIST')) throw err;
    for (let strFolder of arrFolders ) {
        let strPath = strStartPath + '/' + strFolder + '/';
        fs.mkdir(strPath, (err) => {  // создаем структуру папок 01-10. Существование папок - не считаем ошибкой
          if (err && (err.code !== 'EEXIST') ) throw err;
          nFoldersCreated++;
          if (nFoldersCreated == arrFolders.length ) {  // создано ровно 10 папок, можно теперь помещать покемонов
            hidePokemons(strStartPath, arrPokemonsToHide, (objHiddenPokemonList) => { callback(objHiddenPokemonList); }); // начинаем прятать и ждем результат в колбэк вызове
          }
        });
    }
  });
};

function getPokemonsToHide( objPokemonList ) {  // из полного списка выбираем покемонов, которых будем прятать
  let arrPokemonsToHide = new Array();
  let nPokemonsToHide  =  1 + Math.floor( Math.random() * (Math.min(3,  objPokemonList.length ))); // склоько покемонов будем прятать: не больше 3, не больше переданных, случайное число
  for (let i=0; i < nPokemonsToHide; i++) {    // выбираем посчитанное количество покемонов
    let nRandomPokemonPos = Math.floor(Math.random() * objPokemonList.length );
    arrPokemonsToHide.push(objPokemonList[nRandomPokemonPos]);
    objPokemonList.splice(nRandomPokemonPos, 1);
  }
  return arrPokemonsToHide;
}

// прячет покемонов в папки,  выделена в отдельную функцию дял облегчения читаемости кода ( можно было все в hide оставить )
function hidePokemons(strStartPath, arrPokemonsToHide, callback) {
  let arrAvailableFolders = arrFolders; // номера, которые еще не заняты, сначала - все доступно
  let nPokemonsProcessed = 0; // сколько покемонов уже размещено. Чтобы понять "последний" колбэк
  let objHiddenPokemonList = new pc.PokemonList();

  for (let objPokemon of arrPokemonsToHide ) { // начинаем прятать покемонов
    let nRandomFolder = Math.floor(Math.random() * arrAvailableFolders.length );     // выбираем папку, в которую можно положить покемона
    fs.writeFile(strStartPath + '/' +  arrAvailableFolders.splice(nRandomFolder, 1) + '/pokemon.txt', `${objPokemon.name}|${objPokemon.level}`, { encoding: 'utf8' }, err => {
      if (err) throw err;
      objHiddenPokemonList.add(objPokemon.name,objPokemon.level);
      nPokemonsProcessed++;
      if (nPokemonsProcessed == arrPokemonsToHide.length )  callback(objHiddenPokemonList); // обработав последнего покемона - можно передавать управление
    });
  }
}

// получаем влоенные в корневую папки. Там будет 01-10, но все равно соберем их.
const getSubFolders = function(strStartPath)  {
  return new Promise( (resolve, reject) => {
    fs.readdir(strStartPath, (err, files) => {
      if (err) return reject(err);
      resolve(files);
    });
  });
}

// проверим в каких из сложенных папок есть pokemon.txt
const getPokemonFolders = function(strStartPath, folders)  {
let arrPokemonFolders = new Array();
  return new Promise( (resolve, reject) => {
    let nProcessedFolders = 0;
    folders.forEach(strDir => { // проходимся по всем вложенным папкам
      let strPath = strStartPath + '/' + strDir;
      fs.readdir(strPath, (err, files) => { // смотрим вложенные файлы
        if (err) return reject(err);
        nProcessedFolders++;
        if (files[0] && files[0] == 'pokemon.txt') { // предполагаем что в папках могут быть ТОЛЬКО файлы pokemon.txt
          arrPokemonFolders.push(strPath + '/pokemon.txt'); // собираем массив путей ко всем  файлам pokemon.txt
        }
        if (nProcessedFolders == folders.length) { // как только обработали все вложенные папки, готовы отдавать массив.
          resolve(arrPokemonFolders);
        }
      });
    });
  });
}

// считывание информации о всех покемонах из массива папок
const getPokemonsInfo = function( arrPokemonPath )  {
  return new Promise( (resolve, reject) => {
    let nProcessedFiles = 0;
    let arrPokemons = new Array();
    let objPokemonList = new pc.PokemonList();
    arrPokemonPath.forEach(strFile => { // проходимся по всем файлам
      fs.readFile( strFile, {encoding: 'utf8'}, (err, content) => {
        if (err) return reject(err);
        nProcessedFiles++;
        arrPokemons.push(content);
        if (nProcessedFiles == arrPokemonPath.length) { // как только обработали все вложенные папки, готовы отдавать массив.
          arrPokemons.forEach(strPokemon => {
            objPokemonList.add(...strPokemon.split('|'));  // для найденных файлов конвертируем контент в объект
          });
         resolve(objPokemonList);
        }
      });
    });
  });
}

// собираем информацию о покемонах из папок через promises
const seek = ( path => {
  return getSubFolders(path)
    .then( (folders) => getPokemonFolders( path, folders) ) // в каких вложенных папках бужем искать покемонов
    .then( (arrPokemonPath) => getPokemonsInfo( arrPokemonPath) ) // ищем pokemon.txt и собираем содержимое если нашли
    .catch( err => console.log(err))
});

module.exports = {hide, seek};
